# 202307161835 X.Org server

#alpine #config


1. The X.Org server is a display server implementation of the X window system.
   The instructions below were tested on Alpine Linux 3.14 (armv7), 3.17
   (armv7) and 3.18 (x86\_64).

```
# Install desktop bus and udev to load drivers
# and enable them to start at boot
apk add dbus udev
rc-update add dbus default
rc-update add udev default
rc-update add udev-postmount default
rc-update add udev-trigger sysinit

# Install the X server
# xf86-input-synaptics: mouse/trackpad input
# xf86-input-evdev: keyboard input
apk add xorg-server xinit xterm xf86-input-synaptics xf86-input-evdev

# Install suitable video drivers for the hardware display
# e.g. xf86-video-intel, xf86-video-fbdev
# To see the list of available packages: apk search xf86-video-*
apk add xf86-video-fbdev

# Add user to groups: audio input netdev plugdev tty usb video
vim /etc/group

# After installing a desktop environment or window manager,
# add the corresponding executable to $HOME/.xinitrc
# e.g. for i3: exec i3
# To manually start Xorg server, run: startx
```

See also

- [X.Org server errors](202306200001.md)
