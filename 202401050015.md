# 202401050015 Redshift

#alpine


1. Redshift is a utility to adjust display colour temperature based on location
   and time of day.

2. Installation and usage:

```
# Install
apk add redshift

# Download a sample config.
# Edit the config manually to set location coordinates.
mkdir -p ~/.config/redshift
curl -L https://raw.githubusercontent.com/jonls/redshift/master/redshift.conf.sample \
  -o ~/.config/redshift/redshift.conf

# Apply changes in one-shot mode.
# This mode does not continuously adjust the temperature, which may cause
# a flickering effect with some graphics drivers.
# Default day temperature: 6500K (neutral)
# Default night temperature: 4500K
# To reset changes, use the -x flag.
redshift -o

# redshift-gtk is also included for access from a notification tray.
```


Sources

- [jonls/redshift (GPL-3.0-or-later)](https://github.com/jonls/redshift)
- [Redshift - ArchWiki](https://wiki.archlinux.org/title/redshift)
