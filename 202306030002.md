# 202306030002 Mounting USB media: mount + sudo

>> [202306030001](202306030001.md)


Allow users to use the mount/unmount commands without explicitly typing sudo.

```
# Install sudo
apk add sudo

# Add the following to visudo
# Replace vim with a favourite terminal editor
EDITOR=vim visudo
username ALL = NOPASSWD: /bin/mount, /bin/umount

# Create a mount point
mkdir /run/media/[user]/usb
chown -R [user]:[user] /run/media/[user]/usb

# Add the following shell aliases
vim ~/.ashrc
alias mount="sudo mount"
alias umount="sudo umount"

# Mount/unmount
mount /dev/sdXY /run/media/[user]/usb
umount /run/media/[user]/usb
```
