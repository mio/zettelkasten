# 202109050010 APK

#alpine #config


1. Common commands

```
# Add/delete packages
# Use the -a flag to ignore the cache
apk [add|del] [package]

# Install a package from a specific repository
apk add [package] --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing/

# Install a package from a local .apk file
# If installing dependencies from a pinned repository,
# use the -X (or --repository) flag to temporarily pull from the repo
# e.g. -X http://dl-cdn.alpinelinux.org/alpine/edge/testing
apk add --allow-untrusted [package.ver.apk]

# List installed packages
cat /etc/apk/world

# Search packages
apk search [package]

# Get info about a package
apk info [package] -a

# Fix/reinstall a package
apk fix [package]

# Upgrade system
apk upgrade
```

2.  Set up package caching

- [Source](https://wiki.alpinelinux.org/wiki/Local_APK_cache>)

```
# Creates a symlink to /var/cache/apk at /etc/apk
setup-apkcache

# Clean cache
apk -v cache clean

# Download missing packages
apk cache download

# Clear old packages and download missing packages
apk cache -v sync
```

3. Add edge repositories

```
# Edit /etc/apk/repositories
http://dl-cdn.alpinelinux.org/alpine/edge/main
http://dl-cdn.alpinelinux.org/alpine/edge/community
http://dl-cdn.alpinelinux.org/alpine/edge/testing
```

4. Pin a repository

```
# Example to add to /etc/apk/repositories
@testing http://dl-cdn.alpinelinux.org/alpine/edge/testing

# Example of installing a package from the pinned repository
apk add [package]@testing
```

5. Pin a package to a specific version

- [Source](https://superuser.com/questions/1055060/how-to-install-a-specific-package-version-in-alpine)

```
apk add pkgname=pkgver-pkgrel
# Example:
apk add i3wm=4.22-r1
# To undo, uninstall the package and reinstall without the version suffix
```

6. Bootstrap a static APK executable

```
# [mirror]: replace with one from https://nl.alpinelinux.org/alpine/MIRRORS.txt
# [arch]: armv7
# [version]: for the latest version, see https://dl-cdn.alpinelinux.org/alpine/latest-stable/main/armv7/
# e.g. curl -LO https://dl-cdn.alpinelinux.org/alpine/latest-stable/main/armv7/apk-tools-static-2.12.7-r0.apk
curl -LO [mirror]/latest-stable/main/[arch]/apk-tools-static-[version].apk
tar -xzf apk-tools-static-*.apk

# Example usage
./sbin/apk.static -X http://dl-cdn.alpinelinux.org/alpine/latest-stable/main \
  -U --allow-untrusted -p /mnt/chroot --initdb add alpine-base
```


- <https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management>
