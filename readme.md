# About

A notes repository on various topics, with a focus on practical guides.

[Index](000000000000.md)


## License

[CC0](https://creativecommons.org/publicdomain/zero/1.0/)
