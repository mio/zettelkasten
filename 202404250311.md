# 202404250311 Lapis

#lua #alpine


1. Lapis is a web framework for Lua and Moonscript.

2. The following are installation instructions using OpenResty as web server
   and bootstrapping a new project. As of 2024-04-25, OpenResty and LuaJIT
   require Lua 5.1 — newer Lua versions are not yet supported.

```
lver=5.1
apk add openresty lua${lver}-lapis

mkdir [project] && cd [project]
lapis${lver} new

# Add to nginx.conf to avoid "mkdir() /var/tmp/nginx/fastcgi failed
# (13: Permission denied)" errors
error_log /dev/stdout;
pid /tmp/nginx.pid;
server {
  access_log /dev/stdout;
  fastcgi_temp_path /tmp/nginx-fastcgi;
  uwsgi_temp_path /tmp/nginx-uwsgi;
  scgi_temp_path /tmp/nginx-scgi;
}

lapis${lver} serve
```


## Errors

1. Error when calling an etlua template to render.

  ```
  [error] 37155#37155: *1 lua entry thread aborted: runtime error:
  /usr/share/lua/5.1/lapis/application.lua:114: module 'lapis.features.nil' not
  found: [...]
  ```

  Fix: `app:enable("etlua")`. Be careful of syntax error `app.enable`.


## Sources

- [Lapis](https://leafo.net/lapis/)
- [OpenResty](https://openresty.org/)
- [Creating a Lapis Application with Lua](https://leafo.net/lapis/reference/lua_getting_started.html)
- [mkdir() /path/to/dir failed (13: Permission denied)](https://github.com/TrafeX/docker-php-nginx/issues/16#issuecomment-547748903)
