# 202309171738 Re: APKBUILD: Language-specific package notes

>> [202110060002](202110060002.md)


## C

Use samurai with cmake. Example:

```
makedepends="
	cmake
	samurai
	"

build() {
  cd "$builddir"
  cmake -B build -G Ninja \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr
  cmake --build build
}


package() {
  DESTDIR="$pkgdir" cmake --install build
}
```

The example without Ninja:

```
build() {
  cmake -B build \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr
  cd build
  make
}


package() {
  cd "$builddir"/build
  DESTDIR="$pkgdir" make install
}
```

- [Example using samurai: drawpile](https://git.alpinelinux.org/aports/tree/testing/drawpile/APKBUILD)


### Warning when building shared library objects

```
/usr/lib/gcc/i586-alpine-linux-musl/13.1.1/../../../../i586-alpine-linux-musl/bin/ld: warning: creating DT_TEXTREL in a shared object
[...]
TEXTREL  /builds/[user]/aports/testing/[pkgname]/pkg/[...]/[libname].so
```

Potential fix: add PIC flag, e.g. `CCFLAGS="$CCFLAGS -fPIC"`

- Source: [Gentoo Wiki - Textrels Guide](https://wiki.gentoo.org/wiki/Hardened/Textrels_Guide#.22False.22_Positives)


### Troubleshooting "C compiler cannot create executables" errors

Error occurs during `./configure`:

```
configure: error: C compiler cannot create executables
```

- Check the `config.log` for error details.
- Isolate any particular `$CFLAGS` that trigger the error. Stricter flags like `-Werror` and `-Werror=format-security` treat all warnings or certain failed checks as errors respectively.
- Check the C compiler info is detected properly, e.g. version information is emitted correctly and any `$CFLAGS` are passed in without syntax errors.


### Error with printf and string literals

```
error: printf format not a string literal and no format arguments
```

Fix: `printf("%s", variable_name)`

- Source: [c array - warning: format not a string literal](https://stackoverflow.com/questions/9707569/c-array-warning-format-not-a-string-literal)


## Go

Add default flags before the `prepare()` block. Example:

```
export GOFLAGS="$GOFLAGS -modcacherw"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
  go build -v -x -o "$builddir"/bin/
}
```

- [Source](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/50244#note_330945)


## Rust/cargo

Add if applicable.

```
export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_LTO="true"
export CARGO_PROFILE_RELEASE_OPT_LEVEL="s"
export CARGO_PROFILE_RELEASE_PANIC="abort"
```

- [Source](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/33501#note_231696)

- [Rust example using cargo to fetch dependencies: rio](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/51369)
