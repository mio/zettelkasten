" File:        orgroam.vim
" Description: Org-roam filetype detection


autocmd BufNewFile,BufRead,BufEnter *.org set filetype=orgroam
