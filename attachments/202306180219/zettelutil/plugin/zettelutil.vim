" File: zettelutil.vim
" Description: Variables and key mappings for zettelutil


if exists("g:zu_zettelutil")
  finish
endif
let g:zu_zettelutil = 1


" Environment variables
" ----------------------------------------------------------------------------

let g:zu_path = get(g:, 'zu_path', getcwd())
let g:zu_attach_dir = get(g:, 'zu_attach_dir', '/attachments/')
let g:zu_ext = get(g:, 'zu_ext', '.md')
let g:zu_exts = get(g:, 'zu_exts', {})
let g:zu_header = get(g:, 'zu_header', '# ')
let g:zu_id = get(g:, 'zu_id', '%Y%m%d%H%M')
let g:zu_localleader = get(g:, 'zu_localleader', '\\')
let g:zu_re_header = get(g:, 'zu_re_header', 'Re: ')
let g:zu_re_marker = get(g:, 'zu_re_marker', '>> ')
let g:zu_reg = get(g:, 'zu_reg', 'z')


let maplocalleader = g:zu_localleader


" Keybindings
" ----------------------------------------------------------------------------

" Add a new zettel
imap <localleader>za <esc>:call zettel#AddZettel()<esc>i<cr>
nmap <localleader>za :call zettel#AddZettel()<cr>
" Add a new zettel in the background, inserting the link in the current zettel
imap <localleader>zb <esc>:call zettel#AddZettelLink()<esc>i<cr>
nmap <localleader>zb :call zettel#AddZettelLink()<cr>
" Add a new zettel with the filename under the cursor
imap <localleader>zc <esc>:call zettel#AddZettelCursor()<esc>i<cr>
nmap <localleader>zc :call zettel#AddZettelCursor()<cr>

" Add a new directory for the current zettel
nmap <localleader>zd :call zettel#AddZettelDir()<cr>
" Remove a zettel and its directory
" Uses y to confirm, to reduce the possibility of accidental deletion
nmap <localleader>zry :ZUDelZettel [id]

" Go to the zettel in the current buffer
nmap <localleader>zf gF
" Go to the zettel in a new tab
nmap <localleader>zg <c-w>gF
" Add aliases to built-in command for returning to the previous/next buffers
nmap <localleader>zh <c-o>
nmap <localleader>zl <c-i>

" Copy the current zettel id to a register
imap <localleader>zi <esc>:call zettel#CopyZettelInfo('id')<esc>i<cr>
nmap <localleader>zi :call zettel#CopyZettelInfo('id')<cr>
" Copy the current zettel uri to a register
imap <localleader>zu <esc>:call zettel#CopyZettelInfo('link')<esc>i<cr>
nmap <localleader>zu :call zettel#CopyZettelInfo('link')<cr>
" Paste the previously copied contents from the register
imap <localleader>zp <esc>:call zettel#PasteZettelInfo()<esc>i<cr>
nmap <localleader>zp :call zettel#PasteZettelInfo()<cr>

" Search zettels
nmap <localleader>zs :ZUSearchZk term<space>
nmap <localleader>ztg :ZUSearchZk tag<space>
nmap <localleader>zti :ZUSearchZk title<space>
nmap <localleader>zta :ZUSearchZk tags all<cr>
nmap <localleader>ztt :ZUSearchZk titles all<cr>

" Org-roam
nmap <localleader>zof :call orgroam#GoToOrgRoamLink('e')<cr>
nmap <localleader>zog :call orgroam#GoToOrgRoamLink('tabe')<cr>
