" File:        orgroam.vim
" Description: Org-roam syntax settings


if exists('b:current_syntax')
  finish
endif

" Properties block
" syn match orgPropId '\id:\w\{8\}[-]\w\{4\}[-]\w\{4\}[-]\w\{4\}[-]\w\{12\}'
" syn region orgPropBlock start='^:\w\{1,10\}:$' end='^:\w\{1,3\}:$' fold
syn region orgPropBlock start=':PROPERTIES:' end=':END:' fold

" Meta fields, e.g. #+TITLE
" syn match orgMetaValue '.*$' contained oneline
" syn match orgMetaField '^\s*#+\w\+:' nextgroup=orgMetaValue skipwhite
syn match orgMetaTitle '.*$' contained oneline
syn match orgMetaFdTitle '^#+TITLE:' nextgroup=orgMetaTitle skipwhite
syn match orgMetaCat '\w\+' contained oneline
syn match orgMetaFdCat '^#+CATEGORY:' nextgroup=orgMetaCat skipwhite
syn region orgMetaTags start=':\w\{-}\:\{1,}' end='$' contained oneline
syn match orgMetaFdTags '^#+filetags:' nextgroup=orgMetaTags skipwhite

" Source code block definition, e.g. #+begin_src
syn region orgSrcBlock start='^#+begin_src\ \w\+'hs=s+60 end='^#+end_src'he=e-9 fold

" Headings
syn match orgHeadH1 '^\*\{1\}\s.*$'
syn match orgHeadHn '^\*\{2,\}\s.*$'

" Links
" https://github.com/axvr/org.vim
syntax match orgLinkLabel /\[\{2}\([^][]\{-1,}\]\[\)\?[^][]\{-1,}\]\{2}/ containedin=ALL contains=orgLinkLeft,orgLinkRight,orgLinkUri
syntax match orgLinkLeft /\[\{2}/ contained conceal
syntax match orgLinkRight /\]\{2}/ contained conceal
syntax match orgLinkUri /[^][]\{-1,}\]\[/ contains=orgLinkCenter contained conceal
syntax match orgLinkCenter /\]\[/ contained conceal

" (Non-standard) Comments
syn keyword orgTodo contained TODO FIXME NOTE
syn match orgComment '^#\s.*$' contains=orgTodo

" (Non-standard) Caution sign
syn match orgCaution '\[!\]'

" Highlighting
hi def link orgPropBlock    Ignore
hi def link orgMetaTitle    Character
hi def link orgMetaCat      Keyword
hi def link orgMetaTags     Keyword
hi def link orgMetaFdTitle  Identifier
hi def link orgMetaFdCat    Identifier
hi def link orgMetaFdTags   Identifier
hi def link orgSrcBlock     Function
hi def link orgHeadH1       String
hi def link orgHeadHn       Title
hi def link orgLinkUri      Include
hi def link orgLinkLabel    Special
hi def link orgLinkLeft     Keyword
hi def link orgLinkRight    Keyword
hi def link orgLinkCenter   Keyword
hi def link orgTodo         Todo
hi def link orgComment      Comment
hi def link orgCaution      ErrorMsg


let b:current_syntax = 'orgroam'
syn sync fromstart
