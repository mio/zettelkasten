" File: orgroam.vim
" Description: Org-roam handling functions


" Open internal org-roam id links under the cursor
" (Only works for ids of notes inside the same directory)
fun! orgroam#GoToOrgRoamLink(emode)
  let oid =  expand('<cfile>')
  " Look up the file path by id
  let oid_path = system('grep -R ":ID:       ' . oid  . '" ' . getcwd() .
    \ ' | cut -d":" -f 1')
  if !empty(oid_path)
    exe a:emode . ' ' . oid_path
  else
    echo 'No file found by the ID ' . oid . '.'
  endif
endfun
