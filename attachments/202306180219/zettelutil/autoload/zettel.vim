" File: zettel.vim
" Description: zettelkasten-inspired functions


" Set the file extension based on directory paths in g:zu_exts
" e.g. let g:zu_exts = {'/path/to/dir': '.md'}
fun! zettel#SetExt()
  if has_key(g:zu_exts, g:zu_path)
    let g:zu_ext = g:zu_exts[g:zu_path]
  endif
endfun
exe zettel#SetExt()


" Create a new zettel
" https://vi.stackexchange.com/a/8442
fun! zettel#AddZettel()
  let id = strftime(g:zu_id)
  let fp = g:zu_path . '/' . id . g:zu_ext
  " Create a new file with the title pre-inserted
  " Check there is no existing file to avoid overwriting
  if empty(glob(fp))
    silent exe writefile([g:zu_header . id], fp , 'a')
    " Open the new file and put cursor to the end of the line for editing
    exe 'tabe ' . fp
    exe 'normal!ea  '
  else
    " If file already exists, open it for editing
    exe 'tabe ' . fp
    exe 'normal!a'
  endif
endfun

" Create a new zettel with the filename under the cursor
" https://superuser.com/a/277326
fun! zettel#AddZettelCursor()
  let title = expand('<cword>')
  let fp = g:zu_path . '/' . title . g:zu_ext
  if empty(glob(fp))
    silent exe writefile([g:zu_header . title], fp, 'a')
    exe 'tabe ' . fp
    exe 'normal!ea  '
  else
    exe 'tabe ' . fp
    exe 'normal!a'
  endif
endfun

" Insert a new link to the current zettel
fun! zettel#AddZettelLink()
  let id = strftime(g:zu_id)

  let zt_path = g:zu_path . '/' . id . g:zu_ext
  " Preserve cursor position
  exe 'normal m' . g:zu_reg
  " Check if zettel already exists and if not, create it in the background
  " for the built-in gF command to work
  if empty(glob(zt_path))
    " Pre-fill with its new id and the previous zettel's id
    let title = g:zu_header . id . ' ' . g:zu_re_header . expand('%:r')
    " Gemini link
    if g:zu_ext == ".gmi"
      let link = '=> ' . expand('%:t') . ' ' . g:zu_re_marker .
        \ expand('%:r')
    " Markdown link
    else
      let link =  g:zu_re_marker . '[' . expand('%:r') . '](' . expand('%:t') .
        \ ')'
    endif
    silent exe writefile([title, '', link, '', ''], zt_path, 'a')
  endif
  " Gemini link
  if g:zu_ext == ".gmi"
    let clip_link = '=> ' . id . g:zu_ext . ' ' . id
  " Markdown link
  else
    let clip_link = '[' . id . '](' . id . g:zu_ext . ')'
  endif
  silent exe setreg(g:zu_reg, clip_link)
  exe 'normal `' . g:zu_reg
  exe 'put ' . g:zu_reg
endfun

" Copy the current zettel info to the register
fun! zettel#CopyZettelInfo(type)
  exe 'normal m' . g:zu_reg
  if a:type == 'link'
    " Gemini link
    if g:zu_ext == '.gmi'
      let info = '=> ' . expand('%:t') . ' ' . expand('%:r')
    " Markdown link
    else
      let info = '[' . expand('%:r') . '](' . expand('%:t') . ')'<cr>
    endif
  " Id
  else
    let info = expand('%:r')
  endif
  silent exe setreg(g:zu_reg, info)
  exe 'normal `' . g:zu_reg
endfun

" Paste the register contents
fun! zettel#PasteZettelInfo()
  exe 'normal m' . g:zu_reg
  exe 'normal `' . g:zu_reg
  exe 'put ' . g:zu_reg
endfun

" Make a new directory with the current zettel id
" https://stackoverflow.com/a/57093882
fun! zettel#AddZettelDir()
  if empty(glob(g:zu_attach_dir . expand('%:r')))
    silent exe mkdir(g:zu_attach_dir . expand('%:r'))
  endif
endfun

" Delete a zettel and its directory (if any) by id
" https://vim.fandom.com/wiki/Delete_files_with_a_Vim_command
fun! zettel#DelZettel(zid)
  " Delete the file
  if a:zid != '' && !empty(g:zu_path . '/' . a:zid . g:zu_ext)
    silent exe delete(g:zu_path . '/' . a:zid . g:zu_ext)
    echo 'Deleted ' . a:zid . '.'
  endif
  " Delete the directory
  if a:zid != '' && !empty(g:zu_attach_dir . a:zid)
    silent exe delete(g:zu_attach_dir . a:zid, 'rf')
  endif
endfun
command! -nargs=+ ZUDelZettel call zettel#DelZettel(<f-args>)

" Search the zk and paste results into a new tabbed buffer
" For phrases, use \ to escape spaces, e.g. `:SearchZk term software\ dev`
" https://askubuntu.com/a/1198858
fun! zettel#SearchZk(mode, term)
  if a:mode == 'tag'
    let grep_cmd = 'grep -iR "\#' . a:term  . '" ' . g:zu_path
  elseif a:mode == 'tags'
    let grep_cmd = 'grep -iR "^#[a-z]" ' . g:zu_path . ' | sort'
  elseif a:mode == 'title'
    let grep_cmd = 'grep -iR "^#\ [0-9]" ' . g:zu_path . ' | grep -i "' .
      \ a:term . '"'
  elseif a:mode == 'titles'
    let grep_cmd = 'grep -iR "^#\ [0-9]" ' . g:zu_path . ' | sort'
  else
    let grep_cmd = 'grep -iR "' . a:term . '" ' . g:zu_path
  endif
  let time = strftime('%Y%m%d-%H%M%S')
  exe 'tabe ' . g:zu_path . '/.search-' . time . g:zu_ext
  silent exe setreg(g:zu_reg, system(grep_cmd))
  silent exe 'put '. g:zu_reg
endfun
command! -nargs=+ ZUSearchZk call zettel#SearchZk(<f-args>)
