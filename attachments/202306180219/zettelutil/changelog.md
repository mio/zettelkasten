# Changelog


## 2024-01-10

* Fix id and link being reversed when copying and pasting Gemini links
* Update insert mode keybindings


## 2025-01-09

* Fix pasting zettel id in normal mode
* Fix adding a new zettel with the filename indicated by the cursor
* Revise default keybindings
* Copying no longer yanks to the @ register by default


## 2025-01-08

* Fix default settings being unable to be changed
* Use localleader for default keybindings
* Add support for Gemini link syntax


## 2023-06-18

* Initial version
