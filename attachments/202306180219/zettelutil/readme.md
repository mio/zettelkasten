# zettelutil

A Vim plugin to help maintain zettelkasten-style notes.


## Installation

Drop the `zettelutil` directory into `~/.vim/pack/vim-plugins/start/`.


## Usage

See `plugin/zettelutil.vim` for the list of environment variables and default keybindings.


## License

BSD-3-Clause
