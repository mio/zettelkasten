# Alpine Linux on Asus Chromebook C201


This set of scripts can be used to set up an Alpine Linux installation on
external media for the Asus Chromebook C201.

It currently uses the mainline kernel cross-compiled from x86_64 to armv7 with
settings for Veyron Chromebooks.


## Requirements

- USB boot enabled on the Chromebook (see 201710170002)
- Another Alpine Linux installation


## Caution!

The `flash.sh` script will wipe the chosen storage device and replace it with
a new installation. Please check the device path carefully before running the
scripts. Although the scripts should be (mostly) functional, *the scripts are
made available as-is without support. The author(s) and contributor(s) will
not be held liable for any loss of data or other damages as a result of using
the scripts.*


## Usage

1. Create a `cache` directory in the same directory as the scripts. Download
   the kernel, kernel headers and firmware packages into it. Temporarily
   install the public key to the Alpine Linux host installation
   (`/etc/apk/keys`) if needed. Alternatively, build the package APKBUILDs with
   abuild, and place them in `cache`.

2. Put additional `/etc` configs in a new directory named `config`, also in
   the same directory as the scripts, to be copied over to the installation.

3. Adjust the scripts as desired. Change into the directory with the scripts
   and run them in the following order on the host:

- `prepare.sh`: sets up the root filesystem.

- `flash.sh`: prepares the partitions on external media, flashes the kernel
  image to the boot partition and copies the rootfs to the root partition.

4. Default login: `user: root / password: alpine`


### Manual upgrade

The steps in this section are for upgrading to a newer version of the kernel
package used in the scripts.

Theoretically it should also be possible to follow the same method to install
the kernel packages only without using the scripts on another existing
Alpine-based system, but is untested. *Please backup the existing installation
before attempting the steps below.*

```
# Copy the public key used to sign the packages to /etc/apk/keys
# if not already added, to avoid "UNTRUSTED signature" errors.
# It is included with the packages at the source link.

# Install kernel
ver=6.13.0-r0
apk add linux-veyron-cross-$ver.apk

# Optional: install for built-in wifi/bluetooth
apk add linux-firmware-veyron-1.0-r0.apk

# Optional: install for development headers, documentation
apk add linux-veyron-cross-dev-$ver.apk
apk add linux-veyron-cross-doc-$ver.apk

# Flash the kernel image, adjusting for the correct /dev path
dd if=/boot/vmlinuz.kpart of=/dev/mmcblk1p1

# Optional: remove the old kernel packages, e.g.
apk del linux-veyron-cross=$oldver

# Reboot
reboot
```


## What works

Tested working, as of kernel version 6.12.2:

- Bluetooth: file transfer, audio playback
- Sound card, headphone jack
- Video playback (GPU acceleration untested)
- Wireless networking

Unlisted features are untested. Check the Arch Linux ARM or Gentoo documentation
for hardware status.

- [Asus Chromebook Flip C100P - Arch Linux ARM](https://archlinuxarm.org/platforms/armv7/rockchip/asus-chromebook-flip-c100p)
- [Asus Chromebook C201 - Gentoo Wiki](https://wiki.gentoo.org/wiki/Asus_Chromebook_C201)


## Thanks

With thanks to:

- [wsinatra](https://gitlab.com/Durrendal), for patiently reading through text
  walls of terrible ideas and offering various suggestions

- [mps](https://arvanta.net/alpine), for maintaining a solid linux-edge aport,
  along with helpful kernel-related notes


## License

CC0
