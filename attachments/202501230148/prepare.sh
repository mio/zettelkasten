#!/bin/sh
# desc: generate a custom rootfs for Veyron Chromebooks.
# version: 2025-01-26
# depends: abuild
# usage:
# - change into directory with the config: cd /path/to/srcdir
# - make executable on first use: chmod +x ./prepare.sh
# - run: ./prepare.sh


# 3.20 is the last Alpine release before usr-merge
pkgver_alpine=3.20.5
pkgver_kernel=6.13.0
pkgrel_kernel=0
arch="armv7"
source="
  https://cdn.alpinelinux.org/v${pkgver_alpine%.*}/releases/$arch/alpine-minirootfs-$pkgver_alpine-$arch.tar.gz
  https://tilde.team/~mio/veyron/linux-veyron-cross-$pkgver_kernel-r$pkgrel_kernel.apk
  https://tilde.team/~mio/veyron/linux-veyron-cross-dev-$pkgver_kernel-r$pkgrel_kernel.apk
  https://tilde.team/~mio/veyron/linux-firmware-veyron-1.0-r0.apk
"

# alpine-base..uutils-coreutils: core system features
# iptables..wireguard-tools-wg-quick: wireguard
# cronie..procps-ng: process utilities
# cfdisk..umount: disk utilities
# arp-scan..procps-ng: network diagnostic utilities
# tmux..vim: workspace
apks_add="
  alpine-base doas busybox-mdev-openrc wpa_supplicant chrony chrony-openrc
  openssh-client-default uutils-coreutils
  iptables wireguard-tools-wg-quick cronie earlyoom procps-ng
  cfdisk lsblk mount umount arp-scan curl mtr
  tmux vifm vim
"

services_boot="
  bootmisc hostname hwclock local loadkmap modules
  networking seedrng swap sysctl
"
services_default="chronyd cronie earlyoom"
services_shutdown="killprocs mount-ro savecache"
services_sysinit="devfs dmesg mdev"

srcdir="$(pwd)"
cachedir="$srcdir"/cache
tmpdir="$cachedir"/tmp
builddir="$srcdir"/build
configdir="$srcdir"/config
cache_apks=1


# msg|err [message]
msg() { echo -e "$1"; }
err() { echo -e "ERROR: $1"; }


# tidy [dir]
tidy() {
  cd "$1"
  rm -f .PKGINFO .SIGN.* .dummy .pre-* .post-*
}


fetch() {
  test -d "$cachedir" || mkdir -p "$cachedir"
  cd "$cachedir"
  echo "$source" | while IFS= read -r _srcfile; do
    local _srcfile=${_srcfile/  /}
    if [ ! -z "$_srcfile" ]; then
      case "$_srcfile" in
        *http://*|*https://*)
          if [ ! -f "$cachedir"/$(basename "$_srcfile") ]; then
            wget "$_srcfile" || exit 1
          fi ;;
      esac
    fi
  done
  cd "$srcdir"
}


prepare() {
  cd "$srcdir"
  test -d "$builddir" && rm -rf "$builddir"
  mkdir -p "$builddir"
  echo "$source" | while IFS= read -r _srcfile; do
    local _srcfile=${_srcfile/  /}
    if [ ! -z "$_srcfile" ] && [ -f "$cachedir"/$(basename "$_srcfile") ]; then
      msg "Extracting $cachedir/$(basename $_srcfile)"
      case "$_srcfile" in
        *alpine-uboot*)
          test -d "$tmpdir" || mkdir -p "$tmpdir"
          cd "$tmpdir" && tar -xf "$cachedir/$(basename $_srcfile)"
          test -d "$apksdir" || mkdir  -p "$apksdir"
          mv "$tmpdir"/apks/* "$apksdir"/
          rm -r "$tmpdir"
          cd "$srcdir"
          ;;
        *.tar.gz|*.apk|*.tar.xz)
          cd "$builddir" && tar -xf "$cachedir/$(basename $_srcfile)"
          ;;
      esac
    elif [ ! -z "$_srcfile" ] && [ ! -f "$cachedir"/$(basename "$_srcfile") ]; \
      then
      err "Missing $cachedir/$(basename $_srcfile)"
    fi
  done

  tidy "$builddir"
  cd "$srcdir"
}


setup_apks() {
  if [ $cache_apks == 1 ]; then
    cd "$builddir"
    ln -rs var/cache/apk etc/apk/cache
    cd "$srcdir"
  fi
  echo "$apks_add" | while IFS= read -r _line; do
    local _line=${_line/  /}
    for _apk in $_line; do
      apk add --root "$builddir" --arch $arch "$_apk"
    done
  done
}


setup_services() {
  cd "$builddir"
  for _runlevel in boot default shutdown sysinit; do
    test -d "$builddir/etc/runlevels/$_runlevel" || \
      mkdir -p "$builddir/etc/runlevels/$_runlevel"

    local _services=""
    case "$_runlevel" in
      boot) _services="$services_boot" ;;
      default) _services="$services_default" ;;
      shutdown) _services="$services_shutdown" ;;
      sysinit) _services="$services_sysinit" ;;
    esac

    echo "$_services" | while IFS= read -r _line; do
      local _line=${_line/  /}
      for _svc in $_line; do
        msg "Symlinking $_svc at $_runlevel"
        if [ -f "$builddir/etc/init.d/$_svc" ] && \
          [ ! -f "etc/runlevels/$_runlevel/$_svc" ]; then
            ln -rs etc/init.d/"$_svc" "etc/runlevels/$_runlevel/$_svc"
        fi
      done
    done
  done
  cd "$srcdir"
}


setup_configs() {
  cd "$srcdir"
  ls "$configdir" | while IFS= read -r _cfg; do
    if [ -f "$configdir/$_cfg" ] || [ -d "$configdir/$_cfg" ]; then
      msg "Copying $configdir/$_cfg"
      case "$_cfg" in
        apk)
          test -d "$configdir"/"$_cfg"/keys && \
            cp -r "$configdir"/"$_cfg"/keys/* "$builddir"/etc/apk/keys/
          ;;
        home)
          rm -r "$builddir"/home/
          cp -r "$configdir"/"$_cfg" "$builddir"/
          ;;
        *) cp -r "$configdir"/"$_cfg" "$builddir"/etc/ ;;
      esac
    fi
  done
  cd "$srcdir"
}


fetch
prepare
setup_apks
setup_services
setup_configs
