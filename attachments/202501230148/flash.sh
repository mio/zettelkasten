#!/bin/sh
# desc: partition, flash the rootfs and kernel image to external media for
# booting with Veyron Chromebooks.
# version: 2024-12-27
# depends: cgpt coreutils e2fsprogs mount partx rsync umount
# usage:
# - change into directory with rootfs folder: cd /path/to/dir/with/rootfs
# - make executable on first use: chmod +x ./flash.sh
# - run with superuser privileges: ./flash.sh


media="/dev/sda"
mount="/mnt"
kern_pt_label="Kernel"
root_pt_label="Root"
root_fs="$(pwd)/build"
kern_img="$mount/boot/vmlinuz.kpart"

kern_pt="${media}1"
kern_pt_start=8192
kern_pt_size=32768
sec_gpt_table=$(cgpt show "$media" | grep "Sec GPT table" | cut -d" " -f4)
kern_pt="${media}2"
root_pt_start=$(expr $kern_pt_start + $kern_pt_size)
root_pt_size=$(expr $sec_gpt_table - $root_pt_start)


set_pts() {
  case "$media" in
    mmcblk*) # eMMC
      kern_pt="${media}p1"
      root_pt="${media}p2"
      ;;
    *)
      kern_pt="${media}1"
      root_pt="${media}2"
      ;;
  esac
}


part_media() {
  cgpt create "$media"
  cgpt add -i 1 -t kernel -b $kern_pt_start -s $kern_pt_size -l $kern_pt_label \
    -S 1 -T 5 -P 10 "$media"
  cgpt add -i 2 -t data -b $root_pt_start -s $root_pt_size -l $root_pt_label \
    "$media"
  partx -a "$media"
  yes | mkfs.ext4 "$root_pt"
}


sync_media() {
  mount "$root_pt" "$mount"
  rsync -avz "$root_fs/" "$mount"/
  chown -R root:root "$mount"
  dd if="$kern_img" of="$kern_pt"
  umount "$mount"
  sync
}


set_pts
part_media
sync_media
