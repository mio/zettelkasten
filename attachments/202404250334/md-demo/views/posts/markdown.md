# Sample Markdown Post

This is an example of converting Markdown text to HTML using lua-discount for
rendering in Lapis. Only the original Markdown is supported. The conversion seems to mangle parentheses characters in URLs.

## Installation

```
lver=5.1
apk add discount lua${lver}-discount
```

---

## Text attributes

Text attributes _italic_, **bold** and `monospace`.

## Link and image

Footnote-style links where URLs appear at the end of a section are unsupported. Use inline links instead.

[lua-discount](https://asbradbury.org/projects/lua-discount/)

![Markdown mark](https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Markdown-mark.svg/175px-Markdown-mark.svg.png)

## Blockquote

>This is a quote.

## Code block

Triple backticks are unsupported. Use HTML `<code>` and `<pre>` tags instead.

<pre>
app:get("/", function()
  return "Welcome to Lapis " .. require("lapis.version")
end)
</pre>
