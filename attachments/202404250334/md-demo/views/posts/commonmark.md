# Sample Commonmark Post

This is an example of converting [Commonmark] text to HTML using [cmark-lua] for
rendering in Lapis. By default raw HTML tags are omitted.

[Commonmark]: https://commonmark.org/
[cmark-lua]: https://github.com/jgm/cmark-lua


## Installation

```
lver=5.1
apk add libcmark gcc musl-dev lua${lver}-dev luarocks${lver}
luarocks-${lver} install cmark
```

---

## Text attributes

Text attributes _italic_, **bold** and `monospace`.

## Link and image

This is a footer-style link to the [Markdown page on Wikipedia].

[Markdown page on Wikipedia]: https://en.wikipedia.org/wiki/Markdown

![Markdown mark](https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Markdown-mark.svg/175px-Markdown-mark.svg.png)

## Blockquote

>This is a quote.

## Code block

```
app:get("/", function()
  return "Welcome to Lapis " .. require("lapis.version")
end)
```
