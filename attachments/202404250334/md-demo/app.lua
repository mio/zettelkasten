local lapis = require("lapis")
local app = lapis.Application()
app:enable("etlua")


app:get("/", function(self)
  -- Load file contents.
  local md_fh = assert(io.open("views/posts/markdown.md", "r"))
  local md = md_fh:read("*all")
  md_fh.close()
  local cm_fh = assert(io.open("views/posts/commonmark.md", "r"))
  local cm = cm_fh:read("*all")
  cm_fh.close()

  -- (Discount) Convert to HTML.
  -- local discount = require("discount")
  -- self.post = discount(md)

  -- (cmark) Convert to HTML.
  local cmark = require("cmark")
  local cm_parse = cmark.parse_string(cm, cmark.OPT_DEFAULT)
  self.post = cmark.render_html(cm_parse, cmark.OPT_DEFAULT)

  -- Render.
  return { layout = "layout", content_type = "text/html", self.post }
end)

return app
