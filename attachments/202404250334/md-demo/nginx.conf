worker_processes ${{NUM_WORKERS}};
daemon off;
error_log /dev/stdout;
pid /tmp/nginx.pid;

events {
  worker_connections 1024;
}

http {
  include mime.types;

  init_by_lua_block {
    require "lpeg"
  }

  server {
    listen ${{PORT}};
    lua_code_cache ${{CODE_CACHE}};
    access_log /dev/stdout;
    fastcgi_temp_path /tmp/nginx-fastcgi;
    uwsgi_temp_path /tmp/nginx-uwsgi;
    scgi_temp_path /tmp/nginx-scgi;

    location / {
      default_type text/html;
      content_by_lua_block {
        require("lapis").serve("app")
      }
    }

    location /static/ {
      alias static/;
    }

    location /favicon.ico {
      alias static/favicon.ico;
    }
  }
}
