#!/bin/sh


build_kernel() {
  msg "Building kernel"
  msgsub "Installing build, bootloader and cross-compile toolchain"
  apk add git build-base bison flex openssl-dev perl u-boot-tools mkinitfs \
    gcc-arm-none-eabi binutils-arm-none-eabi

  msgsub "Fetching kernel source"
  boot_dir=$src_dir/linux-$kernel_ver/arch/$kernel_arch/boot
  test -d $src_dir || mkdir -p $src_dir
  cd $src_dir
  if [ -d linux-$kernel_ver ]; then
    cd linux-$kernel_ver
    make clean
  else
    git clone $kernel_src -b v$kernel_ver --depth=1 linux-$kernel_ver
    cd linux-$kernel_ver
  fi
  cp -r $config_dir/$defconfig arch/$kernel_arch/configs

  msgsub "Building image from kernel v$kernel_ver"
  # Clear old artifacts of the same kernel version
  test -d $kernel_out && (rm -r $kernel_out && mkdir -p $kernel_out)
  export CROSS_COMPILE=arm-none-eabi-
  case "$model" in
    *c1)
      export ARCH=$kernel_arch
      make $defconfig
      # The c1 bootloader only supports uImage and uInitrd
      make LOADADDR=0x00208000 uImage
      test -f $boot_dir/uImage && cp -r $boot_dir/uImage $kernel_out/uImage
      ;;
  esac

  msgsub "Building device tree and modules"
  make dtbs modules
  INSTALL_MOD_PATH=arch/$kernel_arch make modules modules_install
  case "$model" in
  *c1)
    test -f $boot_dir/dts/meson8b-odroidc1.dtb && \
      (mkdir -p $kernel_out/dtbs && \
      cp -r $boot_dir/dts/meson8b-odroidc1.dtb \
      $kernel_out/dtbs/meson8b-odroidc1.dtb);;
  esac
  test -d arch/$kernel_arch/lib/modules && \
    mkdir -p $kernel_out/lib/modules && (mkdir -p $kernel_out/lib && \
    cp -r arch/$kernel_arch/lib/modules $kernel_out/lib/)

  msgsub "Building headers"
  make headers_install
  test -d usr/include && (mkdir -p $kernel_out/usr && \
    cp -r usr/include $kernel_out/usr/)

  msgsub "Generating initial RAM disk"
  mkinitfs -c gzip -o $boot_dir/initramfs-empty
  mkdir -p $boot_dir/init-tmp
  cd $boot_dir/init-tmp
  gunzip -c $boot_dir/initramfs-empty | cpio -i
  # Populate the fs and replace the modules
  tar xf $src_dir/$(basename $rootfs)
  rm -rf $kernel_out/init-tmp/lib/modules/*
  cp -r $src_dir/linux-$kernel_ver/arch/$kernel_arch/lib/modules \
    $boot_dir/init-tmp/lib/
  # Reassemble into the newc (SVR4 with no CRC) format
  find . | cpio -H newc -o | gzip -9 > $boot_dir/initramfs
  cd $boot_dir
  case "$model" in
    *c1) mkimage -a 22000000 -A $kernel_arch -T ramdisk -C none -n uInitrd \
      -d initramfs uInitrd;;
  esac
  cp -r $boot_dir/uInitrd $kernel_out/uInitrd
  rm -rf init-tmp initramfs-empty
  cd $base_dir
}


. $(pwd)/_env.sh
build_kernel
