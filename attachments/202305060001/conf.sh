#!/bin/sh


vars="$(pwd)/vars_conf"
if [ -f "$vars" ]; then
  . $vars
else
  echo "Error: $vars not found." && exit 1
fi


safe_mount() {
  test -d "$2" || mkdir -p "$2"
  mount "$1" "$2"
}


safe_umount() {
  umount "$1"
  dir_empty="$(ls $1)"
  test -z $dir_empty && rm -r "$1"
}


backup() {
  test -d $config_dir || mkdir -p $config_dir
  for conf in $configs_backup; do
    if [ -f "$conf" ]; then
      test -d $config_dir$(dirname $conf) || \
        mkdir -p $config_dir$(dirname $conf)
      cp $conf $config_dir$conf
    else
      echo "$conf not found, skipping."
    fi
  done
}


restore() {
  test -d $config_dir || (echo "Error: $config_dir not found." && exit 1)
  for conf in $configs_restore; do
    if [ -f "$config_dir$conf" ]; then
      cp $config_dir$conf $conf
    else
      echo "$config_dir$conf not found, skipping."
    fi
  done
}


case "$1" in
  backup) backup;;
  restore) restore;;
  mount) safe_mount "$2" "$3";;
  umount) safe_umount "$2";;
  *) echo "Usage: backup|restore|mount|umount";;
esac
