# Alpine Linux Install Scripts for ODROID-C1

A set of scripts to install Alpine Linux on the ODROID-C1.


## Things to know before use

- **The scripts will erase disk data. Please check the device/mount paths and
  use the scripts carefully.**

- As of current writing, some C1 features do not work in the mainline kernel,
  mainly eMMC, HDMI output, USB OTG (regular USB works). If the features are
  necessary, the alternative is to use the manufacturer's 3.10 kernel. Arch
  Linux ARM also packages the kernel.

- If using a USB wireless adapter, set the kernel version to 5.13 in the config
  variables. Starting with 5.14, some adapters may deauthenticate from a
  network right after joining it, or may not be detected at all in more recent
  kernel versions (6.3 as of last testing).


## Requirements

- Host machine running Alpine: a x86\_64 host will work, it does not need to be
  armv7. It could be used from another distribution with some modifications,
  e.g. replacing references to the APK package manager on the host side with
  the corresponding package manager and package names.

- MicroSD media

- (Optional, recommended) USB UART cable: makes debugging much easier should
  the network connection setup fail.


## Usage 

- Wipe the installation media with a disk utility, e.g. GParted.

- Rename `vars.sample` to `vars` and change the values to suit.

- Run the scripts in the following order:
  - `build.sh`: downloads the Linux kernel source, builds the kernel, device
    tree, modules and header files.
  - `install.sh`: creates new partitions, copies the kernel, flashes the
    bootloader, extracts the filesystem and does some basic configuration.

- It is likely that during the package installation step on the new system
  there will be errors (the init system prefers to be run as suid 0, while
  running via chroot returns a different suid and the init scripts fail). Boot
  into the installation, log in and run `apk fix` with superuser privileges.


### Additional scripts

- `conf.sh`: used after installation, only if there are configuration files
  to be copied over, e.g. from an existing installation.

- `update.sh`: used to copy only the kernel, device tree, modules and
  headers into an existing filesystem.


## License

CC0
