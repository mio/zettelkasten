#!/bin/sh


update_kernel() {
  msg "Mounting and installing to the boot partition"
  test -d $mount || mkdir -p $mount
  mount -t vfat ${dev}1 $mount
  case "$model" in
    *c1)
      cp -r $config_dir/$boot_conf $mount/boot.ini
      cp -r $kernel_out/uImage $mount/
      cp -r $kernel_out/uInitrd $mount/
      cp -r $kernel_out/dtbs $mount/
      ;;
  esac
  umount $mount

  msg "Mounting and installing to the root partition"
  mount -t ext4 ${dev}2 $mount
  cd $mount

  msgsub "Updating modules"
  cp -r $kernel_out/lib/modules $mount/lib/

  msgsub "Updating headers"
  test -d $mount/usr/include && rm -rf $mount/usr/include
  cp -r $kernel_out/usr/include $mount/usr/

  umount $mount
}


update_packages() {
  msg "Updating Alpine"
  mount -t ext4 ${dev}2 $mount

  msgsub "Checking chroot is installed"
  has_chroot="cat /etc/apk/world | grep coreutils"
  test -z "$has_chroot" && apk add coreutils
  test -n $qemu && (cp -r $src_dir/$qemu $mount/usr/bin/ && \
    chmod +x $mount/usr/bin/$qemu)

  msgsub "Mounting chroot bindings"
  mount -o bind /dev $mount/dev
  mount -t proc none $mount/proc
  mount -o bind /sys $mount/sys

  msgsub "(chroot) Running system update"
  chroot $mount $qemu /sbin/apk update
  chroot $mount $qemu /sbin/apk upgrade
  msgsub "**If there are errors, run `apk fix` after logging in.**"

  msgsub "Unmounting chroot bindings"
  test -f $mount/usr/bin/$qemu && rm -r $mount/usr/bin/$qemu
  binds="dev proc sys"
  for bind in $binds; do
    umount $mount/$bind
  done
  umount $mount
}


. $(pwd)/_env.sh
update_kernel
update_packages
