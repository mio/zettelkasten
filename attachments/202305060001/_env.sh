#!/bin/sh


# Paths
base_dir="$(pwd)"
vars="$base_dir/vars"
start_services="$base_dir/start-services.sh"
config_dir="$base_dir/configs"
kernel_dir="$base_dir/kernels"
src_dir="$base_dir/src"
firmware_dir="$src_dir/firmware"


msg() {
  echo -e "$1"
}

msgsub() {
  echo -e "-- $1"
}

err() {
  echo -e "ERROR: $1 Exiting."
  exit 1
}


load_vars() {
  if [ -f $vars ]; then
    msg "Setting environment variables"
    . $vars
  else
    err "$vars not found."
  fi

  # Source urls
  kernel_src="git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git"
  kernel_out="$kernel_dir/$kernel_ver"
  repos="http://dl-cdn.alpinelinux.org/alpine/$repo_ver/main
http://dl-cdn.alpinelinux.org/alpine/$repo_ver/community
http://dl-cdn.alpinelinux.org/alpine/$repo_ver/testing
"
  rootfs_rv=$(echo $rootfs_ver | cut -d "." -f 1-2)
  iso_rv=$(echo $iso_ver | cut -d "." -f 1-2)
  case "$model" in
  *c1) arch="armv7"
    qemu_user="https://github.com/multiarch/qemu-user-static/releases/download/v$qemu_user_ver/qemu-arm-static"
    boot="https://github.com/mdrjr/c1_uboot_binaries/archive/refs/heads/master.tar.gz"
    boot_fdr="c1_uboot_binaries-master"
    boot_conf="$model-boot.ini"
    rootfs="https://dl-cdn.alpinelinux.org/alpine/v$rootfs_rv/releases/armv7/alpine-minirootfs-$rootfs_ver-armv7.tar.gz"
    kernel_arch="arm"
    defconfig="odroidc1_defconfig"
    ;;
  *c2) arch="aarch64"
    qemu_user="https://github.com/multiarch/qemu-user-static/releases/download/v$qemu_user_ver/qemu-aarch64-static"
    boot="http://dn.odroid.com/S905/BootLoader/ODROID-C2/c2_boot_release_20171222.tar.gz"
    boot_fdr="sd_fuse_android"
    boot_conf="$model-boot.scr"
    rootfs="https://dl-cdn.alpinelinux.org/alpine/v$rootfs_rv/releases/aarch64/alpine-minirootfs-$rootfs_ver-aarch64.tar.gz"
    kernel_arch="arm64"
    defconfig="odroidc2_defconfig"
    ;;
  x86_64)
    arch="x86_64"
    iso="https://dl-cdn.alpinelinux.org/alpine/v$iso_rv/releases/x86_64/alpine-standard-$iso_ver-x86_64.iso"
    ;;
  esac
}


get_sources() {
  msg "Fetching sources"
  test -d $src_dir || mkdir -p $src_dir
  cd $src_dir

  host_arch="$(uname -m)"
  if [ ! "$host_arch" == "$arch" ]; then
    msgsub "Fetching qemu-user-static for chroot"
    test -f $(basename $qemu_user) || wget $qemu_user
    qemu=$(basename $qemu_user)
  fi

  case "$arch" in
    armv7|aarch64)
      msgsub "Fetching Alpine mini root filesystem"
      test -f $(basename $rootfs) || wget $rootfs

      msgsub "Fetching prebuilt bootloaders"
      test -f $model-boot.tar.gz || wget -O $model-boot.tar.gz $boot
      if [ -f $model-boot.tar.gz ] && [ ! -d $src_dir/$model-boot ]; then
        tar xf $model-boot.tar.gz
        mv $src_dir/$boot_fdr $src_dir/$model-boot
      fi
      ;;
    x86_64)
      msgsub "Fetching standard ISO"
      test -f $(basename $iso) || wget $iso
      ;;
  esac
  cd $base_dir
}


load_vars
get_sources
