#!/bin/sh


part_disk() {
  msg "Partitioning media"
  msgsub "Checking parted is installed"
  has_parted="cat /etc/apk/world | grep parted"
  test -z "$has_parted" && apk add parted

  msgsub "Creating boot and root partitions"
  parted -s $dev mktable msdos
  # 128M boot partition starts at sector 2048 to leave space for bootloader
  # root partition fills the remaining space on the media
  parted -s $dev unit s -- mkpart primary fat16 2048 264191
  parted -s $dev unit s -- mkpart primary ext4 264192 264193
  parted -s $dev -- resizepart 2 100%
  parted -s $dev -- set 1 boot on
  partprobe $dev
  mkfs.vfat ${dev}1
  mkfs.ext4 ${dev}2
  sync
}


flash_boot() {
  if [ -f $src_dir/$model-boot/sd_fusing.sh ]; then
    msg "Writing bootloader to media"
    # Replace sudo references to allow running from other ACL managers
    sed -i "s/sudo //g" $src_dir/$model-boot/sd_fusing.sh
    # Path needs to be relative for the script to detect the boot files
    cd $src_dir/$model-boot
    ./sd_fusing.sh $dev
    rm -rf $src_dir/$model-boot
  else
    err "$src_dir/$model-boot/sd_fusing.sh not found."
  fi
  cd $base_dir
}


install_fs() {
  msg "Mounting and installing to the boot partition"
  test -d $mount || mkdir -p $mount
  mount -t vfat ${dev}1 $mount
  case "$model" in
    *c1)
      cp -r $config_dir/$boot_conf $mount/boot.ini
      cp -r $kernel_out/uImage $mount/
      cp -r $kernel_out/uInitrd $mount/
      cp -r $kernel_out/dtbs $mount/
      ;;
  esac
  umount $mount

  msg "Mounting and installing to the root partition"
  msgsub "Extracting root filesystem"
  mount -t ext4 ${dev}2 $mount
  rm -rf $mount/*
  cd $mount
  tar xf $src_dir/$(basename $rootfs)
  cd $base_dir

  msgsub "Installing modules"
  cp -r $kernel_out/lib/modules $mount/lib/

  msgsub "Installing headers"
  cp -r $kernel_out/usr/include $mount/usr/

  umount $mount
}


setup_system() {
  msg "Configuring Alpine"
  mount -t ext4 ${dev}2 $mount

  msgsub "Setting up filesystem mounts"
  test -f $config_dir/fstab && \
    mv $mount/etc/fstab $mount/etc/fstab.default && \
    cp -r $config_dir/fstab $mount/etc/

  if [ -n "$modules" ]; then
    msgsub "Adding modules to /etc/modules"
    for mod in $modules; do
      echo $mod >> $mount/etc/modules
    done
  fi

  msgsub "Setting up keymap"
  echo "KEYMAP=\"/usr/share/bkeymaps/$keymap.bmap.gz\"" >> \
    $mount/etc/conf.d/loadkmap

  msgsub "Setting up hostname"
  echo "$hostname" > $mount/etc/hostname

  if [ -f $config_dir/extras/interfaces ]; then
    msgsub "Setting up interfaces"
    cp -r $config_dir/extras/interfaces $mount/etc/network/interfaces
    test -f $config_dir/extras/wpa_supplicant.conf && \
      mkdir -p $mount/etc/wpa_supplicant && \
      cp -r $config_dir/extras/wpa_supplicant.conf $mount/etc/wpa_supplicant/
  fi

  msgsub "Setting up timezone"
  rm -rf $mount/etc/localtime
  ln -s $mount/usr/share/zoneinfo/$tz $mount/etc/localtime

  msgsub "Setting up package repositories"
  echo -e "$repos" > $mount/etc/apk/repositories

  msgsub "Setting up domain name resolvers"
  echo -e "$dns" > $mount/etc/resolv.conf

  msgsub "Checking chroot is installed"
  has_chroot="cat /etc/apk/world | grep coreutils"
  test -z "$has_chroot" && apk add coreutils
  test -n $qemu && (cp -r $src_dir/$qemu $mount/usr/bin/ && \
    chmod +x $mount/usr/bin/$qemu)

  msgsub "Mounting chroot bindings"
  mount -o bind /dev $mount/dev
  mount -t proc none $mount/proc
  mount -o bind /sys $mount/sys

  # # It appears this step requires running commands inside the system to
  # # properly install OpenRC services.
  msgsub "(chroot) Installing packages"
  chroot $mount $qemu /sbin/apk update
  chroot $mount $qemu /sbin/apk add $packages
  msgsub "**If there are errors, run `apk fix` after logging in.**"

  msgsub "(chroot) Linking OpenRC services"
  for service in $run_sysinit; do
    chroot $mount $qemu /sbin/rc-update add $service sysinit
  done
  for service in $run_boot; do
    chroot $mount $qemu /sbin/rc-update add $service boot
  done
  for service in $run_default; do
    chroot $mount $qemu /sbin/rc-update add $service default
  done
  for service in $run_shutdown; do
    chroot $mount $qemu /sbin/rc-update add $service shutdown
  done

  # Setting a root password is required to log in via console.
  msgsub "(chroot) Setting root password"
  echo -e "root:alpine" > $mount/tmp.txt
  test -f $mount/tmp.txt && \
    (chroot $mount $qemu /usr/sbin/chpasswd < $mount/tmp.txt && \
    rm -rf $mount/tmp.txt)

  has_suid="$(ls -l $mount/bin/su | grep suid)"
  if [ -z "$has_suid" ]; then
    msgsub "(chroot) Enabling su"
    chroot $mount $qemu /bin/bbsuid --install
  fi

  if [ -n "$users" ]; then
    msgsub "(chroot) Adding new users"
    for user in $users; do
      chroot $mount $qemu /usr/sbin/adduser -h /home/$user -s /bin/ash $user
      has_user="$(cat $mount/etc/group | grep $user)"
      # Sometimes adduser does not add user to /etc/group
      # Check the home directory as well
      test -z "$has_user" && has_user="$(ls /home | grep $user)"
      test -n "$has_user" && echo -e "$user:alpine" >> $mount/tmp.txt
    done
    test -f $mount/tmp.txt && \
      (chroot $mount $qemu /usr/sbin/chpasswd < $mount/tmp.txt && \
      rm -rf $mount/tmp.txt)
  fi

  msgsub "Unmounting chroot bindings"
  test -f $mount/usr/bin/$qemu && rm -r $mount/usr/bin/$qemu
  binds="dev proc sys"
  for bind in $binds; do
    umount $mount/$bind
  done

  msgsub "Enabling tty console over UART"
  # This is helpful for debugging installations. UART cable required.
  # The inittab also disables other unneeded getty consoles.
  # To not apply the changes, remove the iniitab from the config directory.
  test -f $config_dir/inittab && \
    mv $mount/etc/inittab $mount/etc/inittab.default && \
    cp -r $config_dir/inittab $mount/etc/

  umount $mount
}


. $(pwd)/_env.sh
part_disk
flash_boot
install_fs
setup_system
