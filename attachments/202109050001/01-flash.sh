#!/bin/sh


# Script paths
work_dir=$(dirname "$(readlink -f $0)")
settings_file=$work_dir/00-settings.sh
chroot_script=$work_dir/02-chroot-setup.sh
setup_script=$work_dir/03-config.sh

# Load settings
source $settings_file

# Setup uses 4 packages, with 3 temporarily from Arch Linux ARM:
# alpine-minirootfs - Alpine minimal root filesystem
# linux-veyron, linux-veyron-headers - ChromeOS kernel and optional headers
# firmware-veyron - internal wifi chip firmware
pkg_rootfs=https://dl-cdn.alpinelinux.org/alpine/v3.14/releases/armv7/alpine-minirootfs-3.14.2-armv7.tar.gz
pkg_linux_veyron=http://mirror.archlinuxarm.org/armv7h/core/linux-veyron-3.14.0-26-armv7h.pkg.tar.xz
pkg_linux_veyron_headers=http://mirror.archlinuxarm.org/armv7h/core/linux-veyron-headers-3.14.0-26-armv7h.pkg.tar.xz
pkg_firmware_veyron=http://mirror.archlinuxarm.org/armv7h/core/firmware-veyron-1.0-1-armv7h.pkg.tar.xz
pkg_dir=$work_dir/src

mini_rootfs=$(basename $pkg_rootfs)
linux_veyron=$(basename $pkg_linux_veyron)
linux_veyron_headers=$(basename $pkg_linux_veyron_headers)
firmware_veyron=$(basename $pkg_firmware_veyron)
pkgs="$mini_rootfs $linux_veyron $linux_veyron_headers $firmware_veyron"

conf_dir="$work_dir/configs"
hosts_file=$conf_dir/hosts
resolv_conf=$conf_dir/resolv.conf
ovpn_profile=$conf_dir/default.ovpn
ovpn_client_dir=$fs_mount/etc/openvpn/client


# Set partition names based on the device path
set_part_names() {
  case "$device" in
    /dev/sd*)
      kernel_part=$device"1"
      root_part=$device"2"
      ;;
    /dev/mmc*)
      kernel_part=$device"p1"
      root_part=$device"p2"
      ;;
  esac
}

fetch_packages() {
  echo "Check for packages ..."
  test -d $pkg_dir || mkdir -p $pkg_dir
  for pkg in $pkgs; do
    if [ ! -f $pkg_dir/$pkg ]; then
      echo "Fetching $pkg ..."
      case $(basename $pkg) in
        alpine-minirootfs*) curl -L $pkg_rootfs -o $pkg_dir/$mini_rootfs;;
        linux-veyron*) curl -L $pkg_linux_veyron -o $pkg_dir/$linux_veyron;;
        firmware-veyron*) curl -L $pkg_firmware_veyron \
          -o $pkg_dir/$firmware_veyron;;
      esac
      if [ ! -f $pkg_dir/$pkg ]; then
        echo "Error: missing $(basename $pkg)" && exit 1
      fi
    fi
  done
}

part_device() {
  echo "Creating GPT partition table ..."
  printf "g\nw\n" | fdisk $device

  echo "Creating partitions ..."
  cgpt create $device
  cgpt add -i 1 -t kernel -b 8192 -s 32768 -l Kernel -S 1 -T 5 -P 10 $device
  sec_gpt_table=`cgpt show $device | grep "Sec GPT table" | awk '{print $1}'`
  cgpt add -i 2 -t data -b 40960 -s `expr $sec_gpt_table - 40960` -l Root $device
  partx -a $device

  echo "Formatting root partition ..."
  mkfs.ext4 $root_part
  sync
}

mount_fs() {
  echo "Mounting partition ..."
  test -d $fs_mount || mkdir -p $fs_mount
  [ "$(mount | grep $fs_mount)" ] || mount $root_part $fs_mount
}

unmount_fs() {
  echo "Unmounting partition ..."
  umount $fs_mount
  [ "$(mount | grep $fs_mount)" ] || rm -rf $fs_mount
}

fill_fs() {
  echo "Extracting root file system ..."
  tar -xf $pkg_dir/$mini_rootfs -C $fs_mount 2> /dev/null

  echo "Flashing kernel and copying kernel modules ..."
  mkdir $pkg_dir/linux_veyron
  cd $pkg_dir/linux_veyron && tar -xf $pkg_dir/$linux_veyron 2> /dev/null
  cd $work_dir
  cp -r $pkg_dir/linux_veyron/boot $fs_mount/
  dd if=$pkg_dir/linux_veyron/boot/vmlinux.kpart of="$kernel_part"
  cp -r $pkg_dir/linux_veyron/usr/lib/modules $fs_mount/lib/

  echo "Copying wireless chip firmware ..."
  cp -r $pkg_dir/linux_veyron/usr/lib/firmware $fs_mount/lib/
  cp -r $pkg_dir/linux_veyron/usr/lib/udev $fs_mount/lib/
  rm -rf $pkg_dir/linux_veyron
  # Adjust the firmware path to /lib/firmware, not /usr/lib/firmware
  sed -i "s/\/usr\/lib/\/lib/g" $fs_mount/lib/udev/rules.d/99-veyron-brcm.rules
  mkdir $pkg_dir/firmware_veyron
  cd $pkg_dir/firmware_veyron && tar -xf $pkg_dir/$firmware_veyron
  cd $work_dir
  cp -r $pkg_dir/firmware_veyron/usr/lib/firmware/updates/brcm/* \
    $fs_mount/lib/firmware/brcm/
  rm -rf $pkg_dir/firmware_veyron
}

copy_headers() {
  echo "Copying kernel headers ..."
  mkdir $pkg_dir/linux_veyron_headers
  cd $pkg_dir/linux_veyron_headers && tar -xf $pkg_dir/$linux_veyron_headers \
    2> /dev/null
  cd $work_dir
  cp -r $pkg_dir/linux_veyron_headers/usr/lib/* $fs_mount/lib/
  rm -rf $pkg_dir/linux_veyron_headers
}

copy_scripts() {
  echo "Copying system configuration scripts ..."
  cp $settings_file $fs_mount/
  cp $chroot_script $fs_mount/
  cp $setup_script $fs_mount/

  # Optionally copy network files
  test -f $resolv_conf && cp $resolv_conf $fs_mount/etc/
  test -f $hosts_file && cp $hosts_file $fs_mount/etc/
}

copy_ovpn_config() {
  if [ -f "$ovpn_profile" ]; then
    echo "Copying OpenVPN profile ..."
    mkdir -p $ovpn_client_dir
    cp $ovpn_profile $ovpn_client_dir
  fi
}


set_part_names
fetch_packages
part_device

mount_fs
fill_fs
copy_headers
copy_scripts
copy_ovpn_config
unmount_fs
