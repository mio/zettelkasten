#!/bin/sh


# Check for an alpine-base component and install if not found
check_alpine_base() {
  has_openrc="$(apk info | grep openrc)"
  if [ "$has_openrc" == "" ]; then
    echo "Installing alpine-base for setup tools ..."
    apk add alpine-base
  fi
}

setup_wireless() {
  echo "Updating repo indexes ..."
  apk update

  echo "Setting up wireless interface ..."
  apk add dbus hwids-udev udev wpa_supplicant

  echo "Adding brcmfmac to /etc/modules ..."
  echo "brcmfmac" >> /etc/modules

  # Blacklist btsdio module, which can cause both wireless and Bluetooth
  # to drop connections when the system is suspended
  # https://wiki.gentoo.org/wiki/Asus_Chromebook_C201#Known_issues
  echo "Blacklisting non-functioning btsdio module ..."
  echo "blacklist btsdio" > /etc/modprobe.d/blacklist-btsdio.conf
}

# Assign a list of services to a runlevel
add_inits() {
  echo "Adding service inits to $2 runlevel ..."
  for service in $1; do
    rc-update add $service $2
  done
}

add_services() {
  check_alpine_base

  # https://wiki.alpinelinux.org/wiki/Alpine_Linux_in_a_chroot
  add_inits "devfs dmesg mdev" "sysinit"
  add_inits "hwclock modules sysctl hostname bootmisc syslog" "boot"
  add_inits "mount-ro killprocs savecache" "shutdown"

  # Required for wireless to work on boot
  add_inits "dbus udev udev-trigger" "sysinit"
}

apply_tweaks() {
  echo "Applying minor tweaks to services ..."
  # Temporarily comment out the BPF kernel parameter as it doesn't seem to be
  # recognised by the veyron kernel. This suppresses an error on boot:
  # `sysctl: error: 'kernel.unprivileged.bpf_disabled' is an unknown key`
  sed -i "s/kernel.unprivileged_bpf/#kernel.unprivileged_bpf/" \
    /lib/sysctl.d/00-alpine.conf

  # Disable udev-settle to shorten boot times. It typically waits for 2-4
  # minutes for uevents to be processed before udev-settle eventually fails to
  # start.
  mv /etc/init.d/udev-settle /etc/udev-settle-init-disabled

  # Pre-install kbd-bkeymaps to avoid package install hanging at 46%
  # when running setup-bkeymaps for the first time
  echo "Preloading kbd-bkeymaps for keymap setup ..."
  apk add kbd-bkeymaps
}

add_user() {
  echo "Setting up a new user ..."
  read -p "Enter a username for the new user: " username
  read -p "Enter the preferred shell [ash]: " user_shell
  if [ "$username" == "" ]; then username="alpine"; fi

  # Check if the selected shell is available in the repos
  repo_has_shell=$(apk info | grep "$user_shell")
  if [ ! "$user_shell" == "" ] && [ ! "$repo_has_shell" == "" ]; then
    apk add $user_shell
    adduser -g "$username" -s "/bin/$user_shell" $username
  elif [ ! "$user_shell" == "" ] && [ "$repo_has_shell" == "" ]; then
    echo "Sorry, $user_shell is unavailable. Defaulting to ash ..."
    apk add $user_shell
  else
    adduser -g "$username" $username -s "/bin/ash"
  fi

  read -p "Add $username to sudoers? [y/n]: " if_sudoer
  case "$if_sudoer" in
    y|yes|Yes)
      apk add sudo
      echo "$username ALL=(ALL) ALL" > /etc/sudoers.d/$username \
        && chmod 0440 /etc/sudoers.d/$username
      ;;
  esac
}

setup_wireless
add_services
apply_tweaks
passwd root
add_user
