#!/bin/sh


## 01-flash

# Device path for the new install
# Use `lsblk` to get the device path
device=/dev/sda
# chroot mount path
fs_mount=/tmp/chroot


## 02-chroot

# Chroot mount path
chr_mount=$fs_mount
# chroot shell
shell=/bin/ash


## 03-config

# Wireless network settings
wifi_address="192.168.2.0/24"
wifi_gateway="192.168.2.1"

# Nameservers
# Example uses OpenDNS and Quad9 servers
# More at https://public-dns.info/
dns1="208.67.222.222"
dns2="208.67.220.220"
dns3="9.9.9.9"

# OpenVPN path to client profiles
ovpn_client_dir="/etc/openvpn/client"

# Set locale
# Use `locale -a` to see a list of available locales
locale_charset="en_US.UTF-8"
locale_lang="en_US.UTF-8"

# User groups
# plugdev is required for Networkmanager
groups="audio usb plugdev video"

# CLI tools
core_pkgs="curl lsblk nano"

# All Xorg packages are required for a functioning desktop with graphics,
# keyboard and trackpad input
xorg_pkgs="xf86-video-fbdev xf86-input-evdev xf86-input-synaptics \
  xorg-server xinit xterm"
i3_pkgs="i3wm i3lock i3status feh file-roller gnome-screenshot rofi thunar"
xfce_pkgs="xfce4 file-roller"
desktop="xfce"

# GUI and extra CLI applications
extra_pkgs="pulseaudio pavucontrol evince firefox libreoffice vlc-qt"
testing_pkgs=""
