#!/bin/sh


# Load settings
source $(dirname "$(readlink -f $0)")/00-settings.sh


# Set root partition based on the device path
set_root_part() {
  case "$device" in
    /dev/sd*) root_part=$device"2";;
    /dev/mmc*) root_part=$device"p2"
      ;;
  esac
}

chroot_on() {
  test -d $chr_mount || mkdir -p $chr_mount
  mount $root_part $chr_mount
  mount -o bind /dev $chr_mount/dev
  mount -t proc none $chr_mount/proc
  mount -o bind /sys $chr_mount/sys

  # Copy resolv.conf for name resolution if none exists yet
  test -f $chr_mount/etc/resolv.conf || cp /etc/resolv.conf $chr_mount/etc/

  chroot $chr_mount $shell -l
}

chroot_off() {
  umount $chr_mount/sys $chr_mount/proc $chr_mount/dev
  umount $chr_mount
  [ "$(mount | grep $chr_mount)" ] || rm -rf $chr_mount
}


set_root_part

if [ ! -d "$chr_mount" ]; then
  chroot_on
else
  chroot_off
fi
