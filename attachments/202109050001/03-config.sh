#!/bin/sh
# This script can be used to configure an Alpine installation and install
# additional packages.


# Load settings
source $(dirname "$(readlink -f $0)")/00-settings.sh


# Install packages from stable or testing repos
add_pkgs() {
  echo "Installing $1 package(s) ..."
  case "$1" in
    testing)
      apk add $2 \
        --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing/
      ;;
    *) apk add $2;;
  esac
}

# Check for alpine-base component and install alpine-base for setup-alpine
# if not found
check_alpine_base() {
  has_openrc="$(apk info | grep openrc)"
  if [ "$has_openrc" == "" ]; then
    add_pkgs "setup-alpine" "alpine-base"
  fi
}

# Load a module and add it to /etc/modules
load_module() {
  mod_loaded=$(lsmod | grep "$1")
  mod_startup=$(cat /etc/modules | grep "$1")
  if [ "$mod_loaded" == "" ]; then
    modprobe "$1"
  fi
  if [ "$mod_startup" == "" ]; then
    echo "Adding $1 to /etc/modules ..."
    echo "$1" >> /etc/modules
  fi
}

config_wifi_network() {
  iface="wlan0"
  wpa_conf="/etc/wpa_supplicant/wpa_supplicant.conf"
  iface_conf="/etc/network/interfaces"

  read -p "Set up wifi network? [y/n]: " if_wifi
  case "$if_wifi" in
    y|yes|Yes)
      read -p "Enter the wireless SSID: " wifi_ssid
      echo "Enter the wireless password: "
      stty -echo
      read wifi_password
      stty echo
      wpa_passphrase "$wifi_ssid" "$wifi_password" > $wpa_conf
      cat > $iface_conf <<EOF
auto lo
iface lo inet loopback

auto wlan0
iface $iface inet dhcp
  pre-up wpa_supplicant -B -i $iface -c $wpa_conf
  address $wifi_address
  gateway $wifi_gateway
EOF
      ifup wlan0

      # Add networking service to boot runlevel
      is_network_boot=$(rc-update show | grep "networking")
      if [ "$is_network_boot" == "" ]; then
        rc-update add networking boot
      fi
    ;;
    *) echo "Exiting ..." && exit 0;;
  esac
}

check_internet() {
  read -p "Do you have an internet connection? [y/n]: " has_internet
  case "$has_internet" in
    y|yes|Yes) continue;;
    n|no|No) config_wifi_network;;
    *) "Please ensure you have a working internet connection before running
the script." && exit 1
  esac

  if [ ! -f /etc/resolv.conf ]; then
    echo "Adding nameservers to /etc/resolv.conf ..."
    echo -e "nameserver $dns1\nnameserver $dns2\nnameserver $dns3" \
      > /etc/resolv.conf
  fi

  if [ ! -f /etc/hosts ]; then
    echo "Adding hosts file at /etc/hosts ..."
    cat > /etc/hosts <<EOF
127.0.0.1 localhost
127.0.0.1 localhost.localdomain
127.0.0.1 local
255.255.255.255 broadcasthost
::1 localhost
::1 ip6-localhost
::1 ip6-loopback
0.0.0.0 0.0.0.0
EOF
  fi
}

config_openvpn() {
  add_pkgs "OpenVPN" "openvpn"
  load_module "tun"

  ovpn_profile="default"
  read -p "Enter the OpenVPN profile name (without the .ovpn extension): " \
    ovpn_profile
  if [ "$ovpn_profile" == "" ]; then
    ovpn_profile="default"
  fi

  echo "Adding init service ..."
  ovpn_init="/etc/init.d/openvpn-$ovpn_profile"
  cat > $ovpn_init <<EOF
#!/sbin/openrc-run

name="openvpn-$ovpn_profile"
command="/usr/sbin/openvpn"
command_args="--config $ovpn_client_dir/$ovpn_profile.ovpn"
pidfile="/var/run/openvpn-$ovpn_profile.pid"
start_stop_daemon_args="--background --make-pidfile"

depend() {
  need networking
}
EOF
  chmod +x $ovpn_init

  /etc/init.d/openvpn-$ovpn_profile start
  rc-update add openvpn-$ovpn_profile
}

config_alpine() {
  # Run select alpine-conf scripts instead of setup-alpine. Some steps in
  # setup-alpine were unneeded for the general purpose of this script, and
  # setup-interfaces conflicted with the order of the network setup.
  # https://wiki.alpinelinux.org/wiki/Alpine_setup_scripts
  echo "Running alpine-conf setup scripts ..."
  setup-keymap
  setup-hostname
  rc-service hostname restart --quiet
  setup-timezone

  # Service unit name is crond, not cron
  rc-update add urandom boot
  rc-update add acpid
  rc-update add crond

  setup-apkrepos
  setup-ntp
}

config_locale() {
  # https://wiki.alpinelinux.org/wiki/Installation
  add_pkgs "locale" "musl-locales"

  echo "Enable unicode support in /etc/rc.conf ..."
  sed -i 's/#unicode="NO"\n\n#/#unicode="NO"\n\nunicode="YES"\n\n#/' \
    /etc/rc.conf

  profile_locale=/etc/profile.d/locale.sh.sh
  echo "Adding locales to $profile_locale ..."
  cat > $profile_locale <<EOF
export CHARSET=$locale_charset
export LANG=$locale_lang
export LC_COLLATE=C
EOF
}

config_bluetooth() {
  pkg_brcm_patchram="https://chromium.googlesource.com/chromiumos/third_party/broadcom/+archive/refs/heads/master/bluetooth.tar.gz"
  bpp_build_deps="bluez-dev curl gcc make musl-dev"
  bpp_dir="/tmp/bluetooth"

  add_pkgs "Bluez" "bluez pulseaudio-bluez"
  load_module "btbcm"

  # The packaged brcm_patchram_plus utility from the Arch Linux ARM or
  # kali-arm repos does not allow bluetoothctl to detect the controller, at
  # least in veyron. This step will fetch the third-party Broadcom source,
  # compile it and save the executable to /usr/bin. The utility is called by
  # an udev rule on sysinit to start up the controller.
  echo "Preparing to compile brcm_patchram_plus ..."
  add_pkgs "dev" "$bpp_build_deps"

  echo "Downloading and extracting package ..."
  mkdir $bpp_dir
  curl -L $pkg_brcm_patchram -o $bpp_dir/bluetooth.tar.gz
  cd $bpp_dir && tar -xf bluetooth.tar.gz

  echo "Running make ..."
  # Patch the source
  # https://archlinuxarm.org/packages/armv7h/brcm-patchram-plus/files/0001-fix-includes.patch
  sed -i "s/#include <stdlib.h>/#include <stdlib.h>\n#include <unistd.h>/" \
    $bpp_dir/brcm_patchram_plus.c
  make
  test -f $bpp_dir/brcm_patchram_plus \
    && cp $bpp_dir/brcm_patchram_plus /usr/bin/

  echo "Cleaning up ..."
  rm -rf $bpp_dir
  apk del $bpp_build_deps

  echo "Setting up init services ..."
  btattach_init="/etc/init.d/btattach"
  cat > $btattach_init <<EOF
#!/sbin/openrc-run

name="btattach"
command="/usr/bin/btattach"
command_args="-S 3000000 -B /dev/ttyS0"
pidfile="/var/run/btattach.pid"
start_stop_daemon_args="--wait 5000 --background --make-pidfile"


depend() {
  after coldplug
}
EOF
  chmod +x $btattach_init
  sed -i "s/need dbus/need btattach dbus/" /etc/init.d/bluetooth

  read -p "Start Bluetooth services at boot? [y/n]: " is_bt_boot
  case "$is_bt_boot" in
    y|yes|Yes)
      rc-update add btattach
      rc-update add bluetooth
      ;;
    *)
      echo "To start Bluetooth manually, run: rc-service bluetooth start"
  esac
}

install_desktop() {
  # There is also setup-xorg-base, but it installs extra packages for wider
  # device coverage.
  add_pkgs "X server" "$xorg_pkgs"

  read -p "Enter the username to be added to groups: " username
  if [ "$username" == "" ]; then username="$USER"; fi
  echo "Adding $username to groups ..."
  for group in $groups; do
    adduser $username $group
  done

  read -p "Start desktop on login for $username? [y/n]: " desktop_on_login
  case "$desktop" in
    i3)
      add_pkgs "$desktop" "$i3_pkgs"
      case "$desktop_on_login" in
        y|yes|Yes)
          # i3 needs a shell profile entry to run X separately before i3
          read -p "Enter the shell profile [/home/$username/.ash_profile]: " \
            shell_profile
          if [ "$shell_profile" == "" ]; then
            shell_profile="/home/$username/.ash_profile"
          fi
          cat > $shell_profile <<EOF
if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
  exec startx
fi
EOF
          echo "exec i3" > /home/$username/.xinitrc
          ;;
      esac
      ;;
    *)
      add_pkgs "$desktop" "$xfce_pkgs"
      case "$desktop_on_login" in
        y|yes|Yes) echo "startxfce4" > /home/$username/.xinitrc;;
      esac
      ;;
  esac

  # Allow non-root user to start desktop
  echo -e "allowed_users=anybody\nneeds_root_rights=yes" \
    > /etc/X11/Xwrapper.config
}

# Run after installing all user-facing packages
config_translations() {
  read -p "Get translation packages for all installed package(s)? [y/n]: " \
    get_lang
  case "$get_lang" in
    y|yes|Yes) add_pkgs "translation" "lang";;
  esac
}

# Run after installing all user-facing packages
config_man_pages() {
  read -p "Get application manual pages or documentation? [y/n]: " \
    get_man_pages
  case "$get_man_pages" in
    y|yes|Yes) add_pkgs "man page" "mandoc mandoc-apropos man-pages docs";;
  esac
}


check_internet
check_alpine_base
config_openvpn
config_alpine
config_locale
add_pkgs "core" "$core_pkgs"
config_bluetooth
install_desktop
add_pkgs "testing" "$testing_pkgs"
add_pkgs "extra" "$extra_pkgs"
config_translations
config_man_pages

# Start all services when all apps potentially with service units
# have been installed
openrc boot
openrc default
