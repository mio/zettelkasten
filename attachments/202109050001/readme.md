# Alpine Linux on Asus Chromebook C201


**The scripts in this directory have been deprecated and are left here for
historical reference only. For newer scripts with mainline kernel, please
see [202501230148](../202501230148/readme.md).**


This set of scripts can be used to set up an Alpine file system on external
media for the Asus Chromebook C201. Some of it is lifted from the alpine-conf
scripts (see links below).

It currently uses the veyron kernel via Arch Linux ARM packages. Alpine
packages and mainline kernel may be supported in the future.


## Caution!

The script will wipe the chosen storage device and replace it with a new
installation. Please check the device path carefully before running the
scripts. Although the scripts should be (mostly) functional, *the author(s)
and contributor(s) will not be held liable for any loss of data or other
damages as a result of using the scripts.*

**Please use at your own risk.**


## Requirements

- USB boot enabled on the Chromebook (see below)

- An existing Linux host installation on the device or with the same
  architecture (armv7). This is currently needed to chroot into the OS and set
  up a new user that can be used to log in, as well as bootstrap wireless
  packages if planning to use the built-in wifi chip. The script might work in
  crosh (the ChromeOS developer shell) with some adjustments, but is untested.

- Host system dependencies: `curl cgpt partx util-linux vboot-utils`


## Enable USB boot

- Access recovery mode by pressing `Esc + Refresh/F3 + Power`, then activate
  developer mode (`Ctrl + D`).

- Turn off OS verification, and wait for the system to transition to developer
  mode.

- Reboot and switch to a shell by pressing `Ctrl + Alt + →/F2`.

- Login as `chronos` and obtain superuser privileges: `sudo bash`

- Set the boot parameters below and reboot.

  ```
  crossystem dev_boot_signed_only=0
  crossystem dev_boot_usb=1
  ```


## Usage

- Open `00-settings-sample.sh` and edit the values to suit. Rename the file to
  `00-setting.sh`.

- Run `01-flash.sh` on the host OS to flash the kernel and set up the file
  system.

- Run `02-chroot.sh` on the host OS to chroot into the new file system.
  Inside the chroot, run `/02-chroot-setup.sh` to pre-install a few
  packages and add a new user. Exit the chroot and run `02-chroot.sh` on the
  host OS again to unmount the media.

- After enabling USB boot, boot into the new installation by pressing `Ctrl +
  U` at boot and running `/03-setup.sh` to set up Bluetooth and install other
  packages.


## Links

- [alpine-conf](https://github.com/alpinelinux/alpine-conf)
- [Asus Chromebook Flip C100P - Arch Linux ARM](https://archlinuxarm.org/platforms/armv7/rockchip/asus-chromebook-flip-c100p)
- [Asus Chromebook C201 - Gentoo Wiki](https://wiki.gentoo.org/wiki/Asus_Chromebook_C201)
- [Asus C201 - Debian Wiki](https://wiki.debian.org/InstallingDebianOn/Asus/C201)


## Thanks

Special thanks to [wsinatra](https://gitlab.com/Durrendal) and
[lucidiot](https://git.tilde.town/lucidiot) for their help in getting the
hardware detected and running, as well as answering Alpine-specific questions.


## License

CC0
