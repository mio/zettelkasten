# 202110060003 toAPK

#alpine #dev


1. [toAPK] is a utility to convert Arch Linux PKGBUILD and other similar
   formats to Alpine APKBUILD. The resulting APKBUILD may need to be adjusted
   for architecture, dependencies or other details.


```
# Install toAPK from testing
apk add toapk --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing/

# Generate a config for first-time install
toAPK -c

# If creating an APK package, make a new directory for files
mkdir [package] && cd [package]

# Fetch the PKGBUILD
curl -L https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=[package] \
  -o PKGBUILD

# Convert Arch Linux PKGBUILD to Alpine APKBUILD
toAPK -a /path/to/[package]/PKGBUILD > APKBUILD
```


[toAPK]: https://gitlab.com/Durrendal/toAPK
