# 202305290006 Re: Terminal RSS readers: srss

>> [202305290001](202305290001.md)


1. [/] A dual pane layout takes advantage of widescreen monitors.

2. [-] By default, it seems to present items in chronological order by oldest
   post and ascending, except it doesn't actually check the date stamps, which
   meant that if a feed lists items in ascending order, the order is reversed
   in the TUI to be newest first.

3. [-] Had to scroll through all the items in a feed to reach the next feed.
   Noticed 100% CPU usage while scrolling though the list.

4. [-] Sentences get broken up in the middle of words, which are less
   comfortable to read.


```
apk add git go
git clone https://github.com/sheepla/srss.git
cd srss
go install
cd ..
go/bin/srss -h

# Add feeds by pasting the urls or editing ~/.config/srss/urls.txt
export EDITOR=[editor]
srss e

# Update feed cache
srss u

# Launch the TUI, ctrl-c to exit
srss t
```


- [srss](https://github.com/sheepla/srss)
- License: [MIT](https://github.com/sheepla/srss/blob/master/LICENSE)
