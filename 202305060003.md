# 202305060003 Re: Alpine Linux on ODROID-C1: Disk partitions

>> [202305060001](202305060001.md)


1. Create the disk partitions

```
# Install a disk utility with fat support
apk add dosfstools gparted

# Get the bootloader binaries (links via the manufacturer's website)
mkdir -p bootloaders && bootloaders
wget https://raw.githubusercontent.com/mdrjr/c1_uboot_binaries/master/bl1.bin.hardkernel
wget https://raw.githubusercontent.com/mdrjr/c1_uboot_binaries/master/u-boot.bin
wget https://raw.githubusercontent.com/mdrjr/c1_uboot_binaries/master/sd_fusing.sh
cd ..

# Get the mini root filesystem
wget https://dl-cdn.alpinelinux.org/alpine/v3.17/releases/armv7/alpine-minirootfs-3.17.3-armv7.tar.gz
wget https://dl-cdn.alpinelinux.org/alpine/v3.17/releases/armv7/alpine-minirootfs-3.17.3-armv7.tar.gz.sha256
sha256sum alpine-minirootfs-3.17.3-armv7.tar.gz
cat alpine-minirootfs-3.17.3-armv7.tar.gz.sha256

# Get the target device ID, e.g. /dev/sdc
# Set variables to device and mount paths for convenience
blkid
dev=/dev/sdc
mount=/media/usb

# Create the partitions
# Partition 1 (boot): the kernel image and dtb are about 5.1 MB in total.
# Reserve more space if adding an initramfs. Ensure the partition starts
# at 2048 to provide enough space for the bootloader.
# Use fat16 as the filesystem type and set the lba flag.
# Partition 2 (root): ext4 filesystem will do.
Device  Boot StartCHS    EndCHS        StartLBA     EndLBA    Sectors  Size Id Type
/dev/sdc1    4,4,1       518,5,2           2048     264191     262144  128M  e Win95 FAT16 (LBA)
/dev/sdc2    518,6,1     1023,254,2      264192   31115263   30851072 14.7G 83 Linux
```

2. Set up the boot partition and root filesystem

```
# Mount partition 1
mount -t vfat ${dev}1 $mount
# Copy the uImage, uInitrd (if available), dtb and boot.ini to the top-level
cp -r uImage $mount/
cp -r uInitrd $mount/
cp -r dtb $mount/
cp -r boot.ini $mount/
umount $mount

# Mount partition 2
mount ${dev}2 $mount
# Extract the mini root filesystem tarball into it
tar xf /tmp/odroid-c1/alpine-minirootfs-3.17.3-armv7.tar.gz
# Copy the kernel modules and headers
cp -r /tmp/odroid-c1/lib/modules $mount/lib/modules
rm -r $mount/lib/modules/5.13.0/build
rm -r $mount/lib/modules/5.13.0/source
cp -r /tmp/usr/include $mount/usr/include

# Add any other configuration files as needed, e.g. network interfaces
# Unmount the partition when done
umount $mount
```

3. Bootstrap the bootloader before removing the microSD media and inserting it
   into the device for booting. The binaries include signing keys from the CPU
   manufacturer. The board manufacturer provided a u-boot toolchain to build
   u-boot for the device, but the setup was targeted for the Ubuntu
   distribution and didn't work on Alpine 3.17 when tested. For the above
   reasons, it would be easier to use the manufacturer's prebuilt bootloader.

```
# Make the flashing script executable
cd /tmp/odroid-c1/bootloaders
chmod +x sd_fusing.sh
# The script invokes sudo. If using doas, edit the script
# to replace the sudo references
./fusing.sh $dev
```
